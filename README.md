# pytorch_openpose

## 介绍
用别人的开源项目使用pytorch1.0.0训练了姿态估计项目，使用的是coco2017数据集

## 训练
从[coco](https://cocodataset.org/)数据集官网上下载好相应的数据集后或者直接执行
```shell
./getData.sh
```
然后修改相应程序中的路径，开始训练
```shell
python gen_ignore_mask.py
python train.py
```
相应的训练log文件可见文件中
相应训练的损失变化情况和模型保存可见[https://pan.baidu.com/s/1MRWK7bBwbcxGGfDbgLWHMw](https://pan.baidu.com/s/1MRWK7bBwbcxGGfDbgLWHMw)，提取码为：bhhf，需要将model和log放在work_space中，查看损失变化情况可以下载log文件夹后
```shell
tensorboard --logdir log
```
巡视变化情况如图所示：
<div align="center">
<img src="./imgs/all.png" width="800">
</div>

## 测试
用它给的模型进行测试，结果为下图，相应的模型posenet.pth也可见下载文件中
```shell
python pose_detect.py models/posenet.pth -i data/football.jpg
```
<div align="center">
<img src="./imgs/result.jpg" height="300"><img src="./imgs/result.png" height="300">
</div>

## Reference
https://github.com/TreB1eN/Pytorch0.4.1_Openpose

